/*
| The root of the server.
*/
'use strict';

def.attributes =
{
	// absolute path of the plotle directory
	dir: { type: 'tim:Path' },

	// the servers tim middleware
	timberman: { type: 'Server/Timberman' },
};

def.global = 'root';

const util = require( 'util' );
const http = require( 'http' );

const Timberman = tim.require( 'Server/Timberman' );

/*
| Initializes server root.
|
| ~dir: absolute dir of plotle server
*/
def.static.init =
	async function( dir )
{
	Self.create(
		'dir', dir,
		'timberman', await Timberman.prepare( dir ),
	);

	const handler =
		( request, result ) =>
	{
		root.timberman.requestListener( request, result )
		.catch( ( error ) => { console.error( error ); process.exit( -1 ); } );
	};

	console.log( 'starting server @ http://*/:8833' );
	const server = http.createServer( handler );
	const promise = util.promisify( server.listen.bind( server ) );
	await promise( 8833 );
	console.log( 'server running' );
};
