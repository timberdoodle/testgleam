/*
| Handles adaptions to timberman.
*/
'use strict';

def.attributes =
{
	// the general timberman instance
	_tm : { type : 'timberman:Timberman' }
};

const Timberman = tim.require( 'timberman:Timberman' );
const Sha1 = tim.require( 'timberman:Sha1' );

/*
| To be called by timberman when a resource file changes.
|
| ~timberman:  timbernam instance of the watch registration
| ~name:       name of the resource changed
| ~eventType:  eventType
*/
const notify =
	function( timberman, name, eventType )
{
	console.log( 'Resource ' + name + ' file event; terminating' );
	// rebuilding timberman
	process.exit( 1 );
};

/*
| Adds the roster to timberman.
*/
def.static.addRoster =
	async function( timberman, base )
{
	return await timberman.addResource(
		base,
		{
			name : [ 'index.html', 'devel.html', '' ],
			file : './media/index.html',
			age : 'short',
		},
		{
			name: 'opentype.js',
			file : './node_modules/opentype.js/dist/opentype.js',
			age : 'long',
		},
		{
			name : 'font-DejaVuSans-Regular.ttf',
			file : './font/DejaVuSans-Regular.ttf',
			age : 'long',
		},
	);
};

/*
| Prepares timberman.
| Also builds the bundle.
*/
def.static.prepare =
	async function( adir )
{
	console.log( 'preparing timberman' );

	let tm = Timberman.create( 'log', console.log, 'notify', notify, 'requestCaching', false );

	tm = await Self._addGlobals( tm );

	// prepares resources from the roster
	tm = await tim.addTimbermanResources( tm, [ 'web' ] );
	tm = await Self.addRoster( tm, adir );

	// prepares resources form the the shell
	tm = await tm.addCopse( 'testgleam:Web/Root.js', 'web' );

	// adds the tim catalog init
	tm = tim.addTimbermanCatalog( tm );

	// transducing
	tm = Self._transduce( tm );

	return Self.create( '_tm', tm );
};

/*
| Listens to http requests
| FIXME remove
*/
def.proto.requestListener =
	async function( request, result )
{
	return this._tm.requestListener( request, result );
};

/*
| Applies all transducing.
|
| ~timberman: the timberman to process
*/
def.static._transduce =
	function( timberman )
{
	const resOpentype = timberman.get( 'opentype.js' );
	const hashOpentype = Sha1.calc( resOpentype.data + '' );
	timberman = Self._indexHtml( timberman, hashOpentype );
	timberman = timberman.renameResource( 'opentype.js', 'opentype-' + hashOpentype + '.js' );
	return timberman;
};

/*
| The shell's globals as resource.
*/
def.static._addGlobals =
	async function( timberman )
{
	let text = '';
	const globals = Self._shellGlobals( );
	for( let key of Object.keys( globals ).sort( ) )
	{
		text += 'var ' + key + ' = ' + globals[ key ] + ';\n';
	}

	return(
		await timberman.addResource(
			undefined,
			{
				name : 'global.js',
				data : text,
				list : 'web'
			}
		)
	);
};

/*
| Transducing of index.html.
|
| timberman:    timberman to transduce
| hashOpentype: hash of opentype.js
*/
def.static._indexHtml =
	function( timberman, hashOpentype )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	const res = timberman.get( 'index.html' );
	const devels = [ ];
	for( let name of timberman.getList( 'web' ) )
	{
		devels.push( '<script src="' + name + '" type="text/javascript" defer></script>' );
	}

	let data = res.data + '';
	data = data.replace( /<!--SCRIPTS.*>/, devels.join( '\n' ) );
	data =
		data
		.replace(
			/<!--IMPORTS.*>/,
			'<script src="opentype-' + hashOpentype + '.js" type="text/javascript"></script>'
		);

	return timberman.updateResource( res.create( 'data', data ) );
};

/*
| The shell's globals.
*/
def.static._shellGlobals =
	function( )
{
	return(
		Object.freeze( {
			CHECK: true,
			NODE : false,
			FAILSCREEN : false,
			VISUAL : true,
		} )
	);
};
