/*
| Starts the server.
*/
'use strict';

Error.stackTraceLimit = 15;
//Error.stackTraceLimit = Infinity;

/*
| Sets root as global variable.
| Works around to hide node.js unnessary warning.
*/
Object.defineProperty(
	global, 'root',
	{ configureable: true, writable: true, enumerable: true, value: undefined }
);

// this is node.
global.NODE = true;

// server checking (first true, later override by config)
global.CHECK = true;

const pkg =
	require( '@timberdoodle/tim' )
	.register( 'testgleam', module, 'src/', 'Server/Start.js' );

// registers packages
require( '@timberdoodle/timberman' );
require( '@timberdoodle/gleam' );

pkg.require( 'Server/Root' ).init( pkg.dir );
