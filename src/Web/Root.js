/*
| The root of the web part.
*/
'use strict';

root = undefined;

def.attributes =
{
	canvas: { type: 'protean' },

	display: { type: 'gleam:Display/Canvas' },

	fontFace: { type: 'gleam:Font/Face' },

	resolution: { type: 'gleam:Display/Canvas/Resolution' },
};

def.global = 'root';


//const Angle = tim.require( 'gleam:Angle' );
const Color = tim.require( 'gleam:Color' );
const Display = tim.require( 'gleam:Display/Canvas' );
const Ellipse = tim.require( 'gleam:Ellipse' );
const FigureList = tim.require( 'gleam:Figure/List' );
const Font = tim.require( 'gleam:Font/Root' );
const GlintFigure = tim.require( 'gleam:Glint/Figure' );
const GlintList = tim.require( 'gleam:Glint/List' );
const GlintPane = tim.require( 'gleam:Glint/Pane' );
const GlintString = tim.require( 'gleam:Glint/String' );
const GlintWindow = tim.require( 'gleam:Glint/Window' );
//const Path = tim.require( 'gleam:Path' );
const Point = tim.require( 'gleam:Point' );
//const Ray = tim.require( 'gleam:Ray' );
//const PointList = tim.require( 'gleam:Point/List' );
//const Rect = tim.require( 'gleam:Rect' );
const RoundRect = tim.require( 'gleam:RoundRect' );
const Resolution = tim.require( 'gleam:Display/Canvas/Resolution' );
//const Segment = tim.require( 'gleam:Segment' );
const Size = tim.require( 'gleam:Size' );

let timestampLast;
let timestampStart;
let frameCounter = 0;

let panes = [ ];

/*
| Returns a list of handle shapes.
*/
def.lazy.handleShapes =
	function( )
{
	const hs = Self.pointHandleSize;
	const list = [ ];
	for( let p of this.points ) list.push( Ellipse.CenterSize( p, hs ) );
	return FigureList.Array( list );
};

/*
| Mouse down event.
*/
def.proto.onMouseDown =
	function( event )
{
	const p = Point.FromEvent( event, this.canvas, this.display.resolution );
	const handleShapes = this.handleShapes;
	for( let a = 0, alen = handleShapes.length; a < alen; a++ )
	{
		const hs = handleShapes.get( a);
		if( hs.within( p ) )
		{
			root.create( 'dragging', a );
			return true;
		}
	}
	root.render( );
	return true;
};

/*
| Mouse move event.
*/
def.proto.onMouseMove =
	function( event )
{
	const dragging = this.dragging;
	if( dragging < 0 ) return true;
	const p = Point.FromEvent( event, this.canvas, this.display.resolution );
	root.create( 'points', root.points.set( dragging, p ) );
	root.render( );
	return true;
};

/*
| Mouse up event.
*/
def.proto.onMouseUp =
	function( event )
{
	root.create( 'dragging', -1 );
	root.render( );
	return true;
};


def.proto.frame =
	function( )
{
	let display = this.display;

	/*
	const roundRect =
		RoundRect.create(
			'height', 200,
			'width', 200,
			'pos', Point.XY( 100, 100 ),
			'a', 8,
			'b', 8
		);
	*/

	const g = [ ];
	for( let a = 0, alen = panes.length; a < alen; a++ )
	{
		g.push(
			GlintWindow.create(
				'pane', panes[ a ],
				'pos', Point.XY( a * 50, a * 50 ),
			)
		);
	}

	/*
	g.push( GlintFigure.FigureColor( roundRect, Color.black ) );
	g.push(
		GlintString.create(
			'fontFace', this.fontFace,
			'resolution', this.resolution,
			'string', 'Muhkuh!',
			'p', Point.XY( 30, 50 ),
		)
	);
	*/

	display = display.create( 'glint', GlintList.Array( g ) );
	display.render( );

	root.create(
		'display', display,
	);

	frameCounter++;
	const now = Date.now( );
	if( now - timestampLast > 1000 )
	{
		console.log( frameCounter * 1000 / ( now - timestampStart ) );
		timestampLast = now;
	}

	window.requestAnimationFrame( ( ) => { root.frame( ); } );
};

/*
| Startup of web root.
*/
def.static.startup =
	function( )
{
/**/if( CHECK && root ) throw new Error( );

	let canvas = document.getElementById( 'canvas' );

	const superSampling = 1;
	const size = Size.InnerWindow( );
	const resolution = Resolution.CurrentDevice( superSampling );
	const display =
		Display.AroundHTMLCanvas(
			canvas,
			size,
			resolution,
			pass,
			Color.RGB( 251, 251, 251 ),
		);

	const font = Font.get( 'DejaVuSans-Regular' );
	const fontFace = font.Size( 12 ).Face( Color.black );

	for( let a = 0; a < 10; a++ )
	{
		const pane =
			GlintPane.create(
				'glint',
					GlintList.Elements(
						GlintString.create(
							'fontFace', fontFace,
							'resolution', resolution,
							'string', 'Muhkuh! ' + a,
							'p', Point.XY( 30, 50 ),
						)
					),
				'resolution', resolution,
				'size', Size.WH( 300, 300 )
			);

		panes.push( pane );
	}

	Self.create(
		'canvas', canvas,
		'fontFace', fontFace,
		'display', display,
		'resolution', resolution,
	);

	timestampStart = timestampLast = Date.now( );

	window.requestAnimationFrame( ( ) => { root.frame( ); } );
};

if( !NODE )
{
	window.onload = ( ) =>
	{
		Font.load(
			'DejaVuSans-Regular',
			( font ) =>
		{
			Self.startup( );
		} );
	};
}
