module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es2021": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "globals": {
        "CHECK": true,
        "FAILSCREEN": true,
        "NODE": true,
        "TIM": true,
        "VISUAL": true,
        "config": true,
        "def": true,
        "opentype": true,
        "pass": true,
        "root": true,
        "Self": true,
        "system": true,
        "tim": true
    },
    "parserOptions": {
        "ecmaVersion": 2020
    },
    "rules": {
        "eol-last": "error",
        "indent": [
            "off",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "no-console": "off",
        "no-empty": [ "error", { "allowEmptyCatch": true } ],
        "no-trailing-spaces": "error",
        "no-unused-vars": [
            "error",
            {
                "args" : "none",
                "varsIgnorePattern" : "^tim_proto$"
            },
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
